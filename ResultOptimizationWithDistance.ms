max select all;
max delete;

arr = #()
spheres = #()
k=1;
in_name = "D:\\Coordinate.txt";
in_file = openFile in_name
Rad = readValue in_file
a=sphere pos: [0,0,0] radius: Rad segments:60;	
a.name="MainSph";

if in_file != undefined then
(
	while not(eof in_file)
	do
		(
			x = readValue in_file;
			y = readValue in_file;
			z = readValue in_file;
			temp=[x,y,z]; 
			
			arr[k]=temp;
			r = readValue in_file;
			
		spheres[k]=sphere();
			spheres[k].pos=temp;
			spheres[k].radius=r;
			spheres[k].segments=30;
			spheres[k].name="Sph" +k as string;
			spheres[k].wireColor=random [0,0,0][255,255,255];
			k+=1;
		)	
		close in_file;
 )

 for i=1 to arr.count-1
	 do(
		for j=i+1 to arr.count
			do(
				PointA = arr[i];
				PointB = arr[j];
				newLine = SplineShape pos:pointA
				addNewSpline newLine
				addKnot newLine 1 #corner #line PointA
				addKnot newLine 1 #corner #line PointB
				updateShape newLine

			)
		 )
	 
	 free arr;
 	 free spheres;